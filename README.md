# Salty Dotfiles for Linux Ricing and Seasoning
These dotfiles are meant to also be used alongside my suckless forks:
- (dwm)[https://gitlab.com/FriedTeaCP/tea-dwm]
- (st)[https://gitlab.com/FriedTeaCP/tea-st]
- (dwmblocks)[https://gitlab.com/FriedTeaCP/tea-dwmblocks]
- (dmenu)[https://gitlab.com/FriedTeaCP/tea-dmenu]


- Neovim as the text editor
- CMUS (C Music Player) as the audio player
- Simple Terminal (ST) as the terminal emulator
- Tmux as a terminal multiplexer


## Using a git bare repository to manage your salty dotfiles
Refer to the arch wiki: https://wiki.archlinux.org/index.php/Dotfiles


## Installing/Migrating into a new system
### Salt Mine
(Salt Mine)[https://gitlab.com/FriedTeaCP/salt-mine] is a "program" I wrote that will do the migration for you. The only thing you need to use it is an internet connection (obviously), tar, patch and git.

One liner to download and execute salt mine
```sh
curl -LO https://github.com/FriedTea/salt-mine/releases/download/v1.0.0/mine && chmod +x ./mine && ./mine
```


After migrating/installing, make sure to allow the user to be able to shutdown, reboot, and suspend without a password (Either configuring visudo for sudo or /etc/doas.conf for doas).

### "Dependencies"
These are all really optional, since without them, the "rice" would still be functional:
- hsetroot (Setting the wallpaper)
- xcompmgr or any other compositor for that matter. (For terminal transparency, change the alpha value in the st config.h)
- setxkbmap (To enable keypad mouse/pointer)
- xrandr (Change and grab the screen resolution)
- xset (Change autorepeat value)
- Patched libxft with the bgra patch for emojis on st


## TODO
- [ ] Make a better README
- [X] Add and configure a lockscreen (slock)
- [ ] Add screenshots
- [ ] Decide on a font (bitmap vs normal)
- [X] Setup neovim for native neovim lsp/nvim-lsp + nvim-compe (Snippets, very frustrating to figure out)
- [X] Switch to cmus
- [ ] Make a dwm patch to add a bottom line of the statusbar
- [X] Look into changing tty colors or using 256 colors for salt.vim for the tty
- [X] Change migration script to c (ncurses sweetness) and make it a seperate git repository
- [ ] Add a custom firefox theme : https://github.com/Etesam913/slick-fox/raw/master/userChrome.css

## Recommendations (from me)
1. Mount /tmp as tmpfs. This reduces reads and writes to the HDD/SSD. (Keep in mind that on each reboot everything in /tmp will be deleted)
2. Also read about zram/zswap/zcache which will also reduce reads and writes to the HDD/SSD
3. For people that use github or gitlab, install [hub](https://github.com/profclems/glab) or [cli/gh](https://github.com/cli/cli), both of which are for github, or [lab](https://github.com/zaquestion/lab/) or [glab](https://github.com/profclems/glab) and both are for gitlab
4. If you want a bitmap font, [scientifica](https://github.com/NerdyPepper/scientifica) and [cozette](https://github.com/slavfox/Cozette) seems interesting
